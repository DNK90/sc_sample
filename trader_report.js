const Account = require("./lib/account");
const data = require("./data.json");

// login root
// create new account which acts as trader.
// root grants permission to traders to access:
// - listTraderEnabledPermissions
// - traderReportGetFinishedProductIds
// - getTraderReports
// root must also add some allowed farmId to permissions list

const RootPrivateKey = "0x8843ebcb1021b00ae9a644db6617f9c6d870e5fd53624cefe374c1d2d710fd06";
const RootAddress = "0xc1fe56E3F58D3244F606306611a5d10c8333f1f6";
const rootAccount = new Account({ address: RootAddress, privateKey: RootPrivateKey, ...data.config });

const randomText = (length) => {
  let result = '';
  let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

const makeEmail = (length) => {
  return `${randomText(length)}@${randomText(4)}.${randomText(3)}`;
};

const main = async () => {

  // init new account
  let trader = new Account(data.config);
  let email = makeEmail(6);
  let password = "123456";

  // register
  await trader.register(email, password);
  trader.ID = 0;
  trader.LanguageID = 1;

  // grant permission to trader
  rootAccount.ID = 0;
  await rootAccount.addPermissions({ enabled: ["listTraderEnabledPermissions", "traderReportGetFinishedProductIds", "getTraderReports"] }, [trader.Address]);

  // grant trader permissions of farmIds = [1, 2]
  await rootAccount.sendRequest("updateTraderPermissions",false, "proto.TraderAccessFarmInput", {
      userAddress: trader.Address,
      enabledFarmIds: [1, 2]
    }
  );

  return {
    traderReportGetFinishedProductIds: await trader.queryData("traderReportGetFinishedProductIds",  { languageId: 1 }),
    listTraderEnabledPermissions: await trader.queryData("listTraderEnabledPermissions", { languageId: 1 }),
    getTraderReportsWithFarmIds: await trader.queryData("getTraderReports", { customs: [
        {
          "@type": "proto.TraderReportInput",
          farmIds: [1, 2]
        }
      ], languageId: 1 }),
    getTraderReportsWithFinishedProductIds: await trader.queryData("getTraderReports", { customs: [
        {
          "@type": "proto.TraderReportInput",
          finishedProductIds: [1, 6]
        }
      ], languageId: 1 })
  };
};

main().then((rs) => {
  console.log(JSON.stringify(rs))
}).catch((err) => {
  console.log(err)
});