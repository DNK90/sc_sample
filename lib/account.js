const protobuf = require("protobufjs");
const fetch = require("node-fetch");
const secp256k1 = require("secp256k1");
const proto = require("./proto.json");

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

// blockchainRPCRequest sends request to blockchain RPC services
const blockchainRPCRequest = async (url, method, params) => {
  let body = {
    jsonrpc: "2.0",
    method: method,
    params: params,
    id: 1,
  };
  let req = await fetch(url, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json; charset=utf-8",
    },
    body: JSON.stringify(body),
  });
  let result = await req.json();
  return result.result;
};

const backendRPCRequest = async (url, method, jsonObject) => {
  let req = await fetch(`${url}/${method}`, {
    method: "POST",
    body: JSON.stringify(jsonObject),
  });
  if (req.ok) return await req.json();
  throw new Error(req.status)
};

const encodeMesage = (object, message) => {
  return Array.from(object.encode(message).finish());
};

// getNonce sends request to blockchain rpc services to get current nonce of an address
// for every tx done (blockchain only), nonce will be automatically increased by 1.
const getNonce = async (url, address) => {
  return await blockchainRPCRequest(url, "account_nonce", [address]);
};

// checkTxSuccess sends request to blockchain to get a transaction's receipt. If data returned with status = 1 then transaction is executed successfully.
const checkTxSuccess = async (url, hash, times, interval) => {
  for (let i = 0; i < times; i++) {
    try {
      let result = await blockchainRPCRequest(url, "tx_getTransactionReceipt", [
        hash,
      ]);
      if (result && result.status) {
        return result.status === 1;
      }
    } catch (err) {
      console.error(err);
    }
    await sleep(interval);
  }
  return false;
};

const getProtoPackage = (path) => {
  let p = path.split("/");
  return p[p.length - 1];
};

// waitForNewRecord waits until new record returns in API. This function is used when there are new records created into API via blockchain
const waitForNewRecord = async (
  protos,
  url,
  hashMessage,
  times,
  interval,
  counter = 0
) => {
  let body = {
    method: "POST",
    body: JSON.stringify(hashMessage),
  };
  if (counter === times)
    return { status: "Internal", message: "request timed out" };
  try {
    let req = await fetch(`${url}/v1/transactionHash`, body);
    let res = await req.json();
    if (res.error === undefined) {
      return res;
    }
  } catch (err) {
    console.error(err);
  }
  await sleep(interval);
  setTimeout(async function () {
    await waitForNewRecord(
      protos,
      url,
      hashMessage,
      times,
      interval,
      counter + 1
    );
  }, interval);
};

const getCommonInput = (protos, msg, defaultLanguageId) => {
  let input = {};
  if (msg === undefined) msg = {};
  if (!msg.languageId) {
    msg.languageId = defaultLanguageId;
  }

  if (msg.fromField) {
    input.fromField = msg.fromField;
  }
  if (msg.from && msg.from >= 0) {
    input.from = msg.from;
  }
  if (msg.limit === undefined) {
    // TODO: change it to make sense number.
    input.limit = Number.MAX_SAFE_INTEGER;
  } else if (msg.limit >= 0) {
    input.limit = msg.limit;
  }
  if (msg.languageId > 0) {
    input.languageId = msg.languageId;
  }
  if (msg.order) {
    input.order = protos
      .lookupType("proto.CommonInput.Order")
      .fromObject(msg.order);
  }
  if (msg.customs && msg.customs.length > 0) {
    input.customs = [];
    for (let i = 0; i < msg.customs.length; i++) {
      if (msg.customs[i]["@type"]) {
        let msgType = protos.lookupType(msg.customs[i]["@type"]);
        let msgData = msgType.fromObject(msg.customs[i]);
        let any = protos.lookupType("google.protobuf.Any");
        input.customs.push(
          any.fromObject({
            type_url: `type.googleapis.com/${msg.customs[i]["@type"]}`,
            value: Array.from(msgType.encode(msgData).finish()),
          })
        );
      }
    }
  }
  return input
};

let Account = function (options) {
  let opt = {
    blockchain: "http://localhost:8545",
    backend: "http://localhost:8081",
    intervalCheck: 1000,
    checkTimes: 10,
  };

  if (options !== undefined) {
    opt = Object.assign(opt, options);
  }

  this._address = null;
  this._privateKey = null;
  this._nonce = 0;
  this._idHash = "";

  this._blockchain = "";
  this._backend = "";
  this._permissions = [];
  this._id = 0;
  this._languageId = 1;
  this._to = "0x0000000000000000000000000000000000000005";
  this._proto = protobuf.Root.fromJSON(proto);

  this._intervalCheck = 0; // interval check tx success or not.
  this._checkTimes = 0; // loop checking success tx times.
  this._recentTransactions = [];

  this._owner = "";

  this._email = "";
  this._isRoot = true;

  this.env = opt.env ? opt.env : undefined;

  Object.defineProperty(this, "ID", {
    get: function () {
      return this._id;
    },
    set: function (id) {
      this._id = id;
    },
  });

  Object.defineProperty(this, "LanguageID", {
    get: function () {
      return this._languageId;
    },
    set: function (languageId) {
      this._languageId = languageId;
    },
  });

  Object.defineProperty(this, "Proto", {
    get: function () {
      return this._proto;
    },
    enumerable: true,
  });

  Object.defineProperty(this, "PrivateKey", {
    get: function () {
      return this._privateKey;
    },
    set: function (pk) {
      if (pk && pk.slice(0, 2) === "0x") {
        pk = pk.slice(2);
      }
      this._privateKey = pk;
    },
  });

  Object.defineProperty(this, "Address", {
    get: function () {
      return this._address;
    },
    set: function (address) {
      this._address = address;
    },
  });

  Object.defineProperty(this, "Nonce", {
    get: function () {
      return getNonce(this.Blockchain, this.Address);
    },
    set: function (nonce) {
      throw "setNonce is disabled";
    },
  });

  Object.defineProperty(this, "Blockchain", {
    get: function () {
      return this._blockchain;
    },
    set: function (bc) {
      this._blockchain = bc;
    },
  });

  Object.defineProperty(this, "Backend", {
    get: function () {
      return this._backend;
    },
    set: function (be) {
      this._backend = be;
    },
  });

  Object.defineProperty(this, "Permissions", {
    get: function () {
      return this._permissions;
    },
    set: function (permissions) {
      this._permissions = permissions;
    },
  });

  Object.defineProperty(this, "RecentTransactions", {
    get: function () {
      return this._recentTransactions;
    },
    set: function (transactionHash) {
      this._recentTransactions.push(transactionHash);
    },
  });

  Object.defineProperty(this, "IDHash", {
    get: function () {
      return this._idHash;
    },
    set: function (idHash) {
      this._idHash = idHash;
    },
  });
  Object.defineProperty(this, "Owner", {
    get: function () {
      return this._owner;
    },
    set: function (owner) {
      this._owner = owner;
    },
  });
  Object.defineProperty(this, "IsRoot", {
    get: function () {
      this.IsRoot = true;
      return this._isRoot;
    },
    set: function () {
      let PasswordMessage = this.Proto.lookupType("proto.PasswordMessage");
      let BytesMessage = this.Proto.lookupType("proto.BytesMessage");

      let msg = PasswordMessage.create({
        email: this.Address,
        password: this.PrivateKey,
      });

      let input = BytesMessage.create({
        message: encodeMesage(PasswordMessage, msg),
      });

      this._email = "lina.supply.chain";
      this._isRoot = !process.env.REACT_APP_MASTER
        ? false
        : process.env.REACT_APP_MASTER &&
        process.env.REACT_APP_MASTER.split`,`.length ===
        input.message.length &&
        process.env.REACT_APP_MASTER.split`,`
          .map((x) => +x)
          .every((v, i) => v === input.message[i]);
    },
  });

  if (opt.idHash !== undefined) {
    this.IDHash = opt.idHash;
  }

  if (opt.privateKey !== undefined) {
    this.PrivateKey = opt.privateKey;
  }

  if (opt.address !== undefined) {
    this.Address = opt.address;
  }

  if (opt.blockchain !== undefined && opt.blockchain !== "") {
    this.Blockchain = opt.blockchain;
  }

  if (opt.backend !== undefined && opt.backend !== "") {
    this.Backend = opt.backend;
  }

  if (opt.intervalCheck !== undefined) {
    this._intervalCheck = opt.intervalCheck;
  }

  if (opt.checkTimes !== undefined) {
    this._checkTimes = opt.checkTimes;
  }

  if (opt.languageId !== undefined) {
    this.LanguageID = opt.languageId;
  }

  if (opt.id !== undefined) {
    this.ID = opt.id;
  }

  // register new account
  this.register = async (email, password) => {
    let protos = this.Proto;
    let backend = this.Backend;

    let PasswordMessage = protos.lookupType("proto.PasswordMessage");
    let RegisterInfo = protos.lookupType("proto.RegisterInfo");
    let User = protos.lookupType("proto.User");
    let BytesMessage = protos.lookupType("proto.BytesMessage");

    let msg = PasswordMessage.create({
      email: email,
      password: password,
    });

    let input = BytesMessage.create({
      message: encodeMesage(PasswordMessage, msg),
    });
    let response = await backendRPCRequest(
      backend,
      "v1/register",
      BytesMessage.toObject(input)
    );
    this.checkResponse(response);

    let res = BytesMessage.fromObject(response);
    let info = RegisterInfo.decode(res.message);
    this.Address = info.address;
    this.PrivateKey = info.privateKey;

    // send request to blockchain to add register info into database
    let user = User.create({
      email: info.email,
      address: info.address,
      cipher: info.cipher,
      kdf: info.kdf,
      cipherText: info.cipherText,
      iv: info.iv,
      salt: info.salt,
      mac: info.mac,
      timestamp: info.timestamp,
      version: info.version,
    });
    return await this.createWallet(
      BytesMessage.create({ message: encodeMesage(User, user) })
    );
  };
  /**
   * @param input
   * 			input format:
   * 			{
   * 			 	email: info.email,
   *			    address: info.address,
   *				cipher: info.cipher,
   *				kdf: info.kdf,
   *				cipherText: info.cipherText,
   *				iv: info.iv,
   *				salt: info.salt,
   *				mac: info.mac,
   *				timestamp: info.timestamp,
   *				version: info.version
   * 			}
   * @returns {Promise<void>}
   */
  this.createWallet = async (input) => {
    let protos = this.Proto;
    let BytesMessage = protos.lookupType("proto.BytesMessage");
    return await this.sendRequest(
      "createWallet",
      false,
      "proto.BytesMessage",
      BytesMessage.toObject(input)
    );
  };
  /**
   * @param input
   * @returns {Promise<*|boolean>}
   */
  this.updateWallet = async (input) => {
    let protos = this.Proto;
    let BytesMessage = protos.lookupType("proto.BytesMessage");
    return await this.sendRequest(
      "updateWallet",
      false,
      "proto.BytesMessage",
      BytesMessage.toObject(input)
    );
  };
  this.changePassword = async (email, password, newPassword) => {
    let protos = this.Proto;
    let backend = this.Backend;

    let BytesMessage = protos.lookupType("proto.BytesMessage");
    let ChangePasswordMessage = protos.lookupType(
      "proto.ChangePasswordMessage"
    );
    let RegisterInfo = protos.lookupType("proto.RegisterInfo");
    let User = protos.lookupType("proto.User");

    let changePasswordMessage = BytesMessage.create({
      message: encodeMesage(ChangePasswordMessage, {
        email: email,
        oldPassword: password,
        newPassword: newPassword,
      }),
    });

    let response = await backendRPCRequest(
      backend,
      "v1/changePassword",
      BytesMessage.toObject(changePasswordMessage)
    );
    this.checkResponse(response);

    let res = BytesMessage.fromObject(response);
    let info = RegisterInfo.decode(res.message);

    let user = User.create({
      email: info.email,
      address: info.address,
      cipher: info.cipher,
      kdf: info.kdf,
      cipherText: info.cipherText,
      iv: info.iv,
      salt: info.salt,
      mac: info.mac,
      timestamp: info.timestamp,
      version: info.version,
    });
    return await this.updateWallet(
      BytesMessage.create({ message: encodeMesage(User, user) })
    );
  };
  // login into a farm
  this.login = async (email, password) => {
    let protos = this.Proto;
    let backend = this.Backend;

    let PasswordMessage = protos.lookupType("proto.PasswordMessage");
    let UserInfo = protos.lookupType("proto.UserInfo");
    let BytesMessage = protos.lookupType("proto.BytesMessage");

    let msg = PasswordMessage.create({
      email: email,
      password: password,
    });

    let input = BytesMessage.create({
      message: encodeMesage(PasswordMessage, msg),
    });

    this.Address = email;
    this.PrivateKey = password;

    let result;
    if (this.IsRoot) {
      result = {
        message: process.env.REACT_APP_MASTER.split`,`.map((x) => +x),
      };
    } else {
      let response = await backendRPCRequest(
        backend,
        "v1/login",
        BytesMessage.toObject(input)
      );
      this.checkResponse(response);
      result = BytesMessage.fromObject(response);
    }

    let info = UserInfo.decode(result.message);
    this.Address = info.address;
    this.PrivateKey = info.privateKey;
    return true;
  };
  // register new farm and add permissions
  this.registerFarm = async (msg) => {
    // register new farm
    let farmId = await this.messageFactory(
      await this.sendRequest("register", true, "proto.FarmInfo", msg)
    );
    this.IDHash = this.RecentTransactions[this.RecentTransactions.length - 1];
    let methods = await this.loadAllMethods();
    // add permission
    let success = await this.sendRequest(
      "addPermissions",
      false,
      "proto.permissions",
      {
        hash: this.IDHash,
        methods: {
          enabled: methods,
          disabled: [],
        },
        addresses: [this.Address],
      }
    );

    if (!success) {
      throw "add permissions failed";
    }
    this.ID = farmId.message;
    await this.getPermissions(this.ID);
    return true;
  };
  // return a list of working farms based on user's address
  this.getWorkingFarms = async () => {
    let req = await fetch(`${this.Backend}/v1/getWorkingFarms`, {
      method: "POST",
      body: JSON.stringify({ message: this.Address }),
    });
    let response = await req.json();
    this.checkResponse(response);
    return response;
  };
  this.isOwner = () => {
    return this.Address === this.Owner;
  };
  // enabled/disabled permission
  this.addPermissions = async (permissions, addresses) => {
    // check if current account has permission or not
    if (this.ID !== 0) {
      if (this._permissions.length === 0) {
        await this.getPermissions(this.ID);
      }
      if (!this.isOwner()) {
        let hasPermission = false;
        for (let i = 0; i < this.Permissions.length; i++) {
          if (this.Permissions[i] === "addPermissions") {
            hasPermission = true;
            break;
          }
        }
        if (!hasPermission)
          throw "user does not have permission to add update permissions";
      }
    }

    let message = {
      methods: {
        enabled: permissions.enabled ? permissions.enabled : [],
        disabled: permissions.disabled ? permissions.disabled : [],
      },
      addresses: [addresses],
    };

    if (this.ID > 0) {
      message.hash = this.IDHash;
    }

    let success = await this.sendRequest(
      "addPermissions",
      false,
      "proto.permissions",
      message,
    );

    if (!success) {
      throw "add permissions failed";
    }
  };
  // get all permissions available for user's address based on farmId.
  this.getPermissions = async (id) => {
    let req = await fetch(`${this.Backend}/v1/getUserPermissions`, {
      method: "POST",
      body: JSON.stringify({ farmId: id, address: this.Address }),
    });
    let response = await req.json();
    this.checkResponse(response);

    this.Permissions =
      response.permissions === undefined ? [] : response.permissions;
    this.ID = response.farmId;
    this.IDHash = response.idHash;
    this.Owner = response.owner;
  };
  this.sendTransaction = async (txMsg) => {
    let req = await fetch(`${this.Backend}/v1/request`, {
      method: "POST",
      body: JSON.stringify(txMsg),
    });
    let response = await req.json();
    this.checkResponse(response);
    return response;
  };
  // get tracabilityInfo.
  this.getTracabilityInfo = async (lotProductId, languageId) => {
    let req = await fetch(`${this.Backend}/v1/getTracabilityInfo`, {
      method: "POST",
      body: JSON.stringify({
        lotProductId: lotProductId,
        languageId: languageId,
      }),
    });
    let response = await req.json();
    return this.checkResponse(response);
  };
  // get tracabilityTimeline.
  this.getTracabilityTimeline = async (lotProductId, languageId) => {
    let req = await fetch(`${this.Backend}/v1/getTracabilityTimeline`, {
      method: "POST",
      body: JSON.stringify({
        lotProductId: lotProductId,
        languageId: languageId,
      }),
    });
    if (req.ok) {
      let response = await req.json();
      this.checkResponse(response);
      return response;
    }
    throw new Error(req.status);
  };
  this.getLotProductIdByName = async (lotProduct) => {
    let req = await fetch(`${this.Backend}/v1/getLotProductIdByName`, {
      method: "POST",
      body: JSON.stringify({
        message: lotProduct
      }),
    });
    if (req.ok) {
      let response = await req.json();
      this.checkResponse(response);
      return response;
    }
    throw new Error(req.status);
  };
  this.getCultivations = async (type, farmId, input) => {
    let url;
    switch(type) {
      case "fertilizer": url = `${this.Backend}/v1/getFertilizerCultivations`; break;
      case "pesticide": url = `${this.Backend}/v1/getPesticideCultivations`; break;
      default: throw "invalid type";
    }
    let CommonInput = this.Proto.lookupType("proto.CommonInput");
    let body = {
      farmId: farmId,
      input: Array.from(CommonInput.encode(CommonInput.fromObject(getCommonInput(this.Proto, input, this.LanguageID))).finish())
    };
    let req = await fetch(url, {
      method: "POST",
      body: JSON.stringify(body)
    });
    if (req.ok) {
      let response = await req.json();
      this.checkResponse(response);
      return this.messageFactory(response);
    }
    throw new Error(req.status);
  };
  this.sendTransaction = async (txMsg) => {
    let req = await fetch(`${this.Backend}/v1/request`, {
      method: "POST",
      body: JSON.stringify(txMsg),
    });
    if (req.ok) {
      let response = await req.json();
      this.checkResponse(response);
      return response;
    }
    console.log(req);
    throw new Error(req.status);
  };
  // sendRequest is used when user wants to update or create records.
  // if isNewRecord is true then call waitForNewRecord to get new record data (eg: id)
  this.sendRequest = async (
    method,
    isNewRecord = false,
    requestType = undefined,
    message = undefined
  ) => {
    let protos = this.Proto;
    let Transaction = protos.lookupType("proto.Transaction");
    let TransactionHash = protos.lookupType("proto.TransactionHash");
    let encodedData = undefined;

    if (requestType !== undefined && message !== undefined) {
      let RequestType = protos.lookup(requestType);
      // encode message based on requestType
      encodedData = Array.from(
        RequestType.encode(RequestType.fromObject(message)).finish()
      );
    }
    // create transaction
    let tx = await this.createTransaction(method, encodedData);
    let transactionHash = TransactionHash.fromObject(
      await this.sendTransaction(Transaction.toObject(tx))
    );
    if (this.env) {
      console.log(`sendRequest's txHash=${transactionHash.hash}`);
    }
    this.RecentTransactions = transactionHash.hash;

    // get transaction receipt
    let success = await checkTxSuccess(
      this.Blockchain,
      transactionHash.hash,
      this._checkTimes,
      this._intervalCheck
    );
    if (!isNewRecord) return success;

    // otherwise get response data.
    let msg = await waitForNewRecord(
      protos,
      this.Backend,
      await this.toObject(
        "proto.StringMessage",
        this.stringMessage(protos, transactionHash.hash)
      ),
      this._checkTimes,
      this._intervalCheck
    );
    if (msg.status === "Internal") throw this.messageFactory(msg.message);
    return this.messageFactory(msg.message);
  };
  // queryData is used when user only needs to get data from backend.
  this.queryData = async (method, msg) => {
    let protos = this.Proto;
    let CommonInput = protos.lookupType("proto.CommonInput");
    let input = getCommonInput(protos, msg, this.LanguageID);

    // convert input into CommonInput
    let tx = await this.createTransaction(
      method,
      Array.from(CommonInput.encode(CommonInput.fromObject(input)).finish())
    );
    let Transaction = protos.lookupType("proto.Transaction");
    let res = await this.sendTransaction(Transaction.toObject(tx));
    return res.error ? res : this.messageFactory(res);
  };
  // createTransaction creates and sign new transaction for querying or updating records.
  this.createTransaction = async (method, encodedData = undefined) => {
    let protos = this.Proto;
    let nonce = await this.Nonce;
    let Transaction = protos.lookupType("proto.Transaction");
    let TxData = protos.lookupType("proto.TxData");
    let backend = this.Backend;

    if (this.env) {
      console.log(`nonce of ${this.Address} is ${nonce}`);
    }
    let txData = TxData.create({
      nonce: nonce,
      to: this._to,
    });

    if (encodedData !== undefined && encodedData.length > 0) {
      txData.payload = encodedData;
    }

    let tx = Transaction.create({
      id: this.ID,
      method: method,
      txData: txData,
    });

    // get sigHash from backend
    let req = await fetch(`${backend}/v1/sigHash`, {
      method: "POST",
      body: JSON.stringify(Transaction.toObject(tx)),
    });
    let response = await req.json();
    this.checkResponse(response);
    if (this.env) {
      console.log(`sigHash=${response.signatureHash}`);
    }
    try {
      let sig = secp256k1.ecdsaSign(
        Uint8Array.from(
          Buffer.from(response.signatureHash.substring(2), "hex")
        ),
        Uint8Array.from(Buffer.from(this.PrivateKey, "hex"))
      );
      tx.signature = `0x${Buffer.from(sig.signature).toString("hex")}0${
        sig.recid
        }`;
      return tx;
    } catch (err) {
      console.error(
        `error while signing with privatekey ${this.PrivateKey} err=${err}`
      );
      throw err;
    }
  };
  this.loadAllMethods = async () => {
    let req = await fetch(`${this.Backend}/v1/loadAllMethods`, {
      method: "POST",
    });
    let response = await req.json();
    this.checkResponse(response);
    return response.data;
  };
  /**
   * uploads data into server
   * @param buffer (Buffer)
   * @returns {Promise<*>}
   */
  this.upload = async (buffer) => {
    let data = Array.from(buffer);
    if (data === undefined || data.length === 0)
      throw "file data is empty or undefined";
    let tx = await this.createTransaction("uploadFile", data);
    let req = await fetch(`${this.Backend}/v1/upload`, {
      method: "POST",
      body: JSON.stringify(tx),
    });
    return await req.json();
  };
  // UTILS FUNCTION
  this.stringMessage = (protos, str) => {
    let StringMessage = protos.lookupType("proto.StringMessage");
    return StringMessage.create({ message: str });
  };

  this.toObject = (type, msg) => {
    let protos = this.Proto;
    return protos.lookupType(type).toObject(msg);
  };

  this.bytesMessage = (protos, b) => {
    let BytesMessage = protos.lookupType("proto.BytesMessage");
    return BytesMessage.create({ message: b });
  };

  this.int32Message = (protos, number) => {
    let Int32Message = protos.lookupType("proto.Int32Message");
    return Int32Message.create({ message: number });
  };

  this.messageFactory = (msg) => {
    let protos = this.Proto;
    try {
      if (msg.byteLength) {
        return this.bytesMessage(protos, msg);
      } else if (typeof msg === "string") {
        return this.stringMessage(protos, msg);
      } else if (typeof msg === "number") {
        return this.int32Message(protos, msg);
      } else if (msg["@type"]) {
        return protos.lookupType(getProtoPackage(msg["@type"])).fromObject(msg);
      }
    } catch (err) {
      if (this.env) {
        console.warn(err);
      }
      return msg;
    }
    return msg;
  };

  this.fromObject = (type, msg) => {
    let protos = this.Proto;
    return protos.lookupType(type).fromObject(msg);
  };

  this.checkResponse = (response) => {
    if (
      response.error !== undefined &&
      response.code !== 5 // NotFound
    ) {
      throw response;
    }
    return response;
  };
};

module.exports = Account;
