const Account = require("./lib/account");

const data = require("./data.json");
const RootPrivateKey = "0x8843ebcb1021b00ae9a644db6617f9c6d870e5fd53624cefe374c1d2d710fd06";
const RootAddress = "0xc1fe56E3F58D3244F606306611a5d10c8333f1f6";
const fs = require('fs');

// let ts = require("google-protobuf/google/protobuf/timestamp_pb");

const randomText = (length) => {
	let result = '';
	let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	let charactersLength = characters.length;
	for (let i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
};

const randomNumbers = (length) => {
	let result = '';
	let characters = '123456789';
	let charactersLength = characters.length;
	for (let i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
};

const randomDateStart = () => {
	const start = new Date(2019, Math.random() * 12, (Math.random() * 28) + 1);
	return { seconds: Math.floor(start.getTime()/1000), nanos: start.getMilliseconds() };
};

const randomDateEnd = () => {
	const start = new Date();
	let end = new Date(2020 + (Math.random() * 5), 1, 1);
	end = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
	return { seconds: Math.floor(end.getTime()/1000), nanos: end.getMilliseconds() };
};

const makeEmail = (length) => {
	return `${randomText(length)}@gmail.com`;
};

const setupMetadata = async () => {
	// root account
	// add meta data. such as: language
	let rootAccount = new Account({ address: RootAddress, privateKey: RootPrivateKey, ...data.config });
	let vietnamId = rootAccount.messageFactory(await rootAccount.sendRequest("addLanguage", true, data.requestType.language, { language: "Vietnamese" }));
	let englishId = rootAccount.messageFactory(await rootAccount.sendRequest("addLanguage", true, data.requestType.language, { language: "English" }));
	let unitId = rootAccount.messageFactory(await rootAccount.sendRequest("addUnit", true, data.requestType.unitInfo, { name: "unit1", languageId: vietnamId.message }));
	let certificateId = rootAccount.messageFactory(await rootAccount.sendRequest("addCertificate", true, data.requestType.certificateInfo, {name: randomText(3)}));
	let soilId = rootAccount.messageFactory(await rootAccount.sendRequest("addSoil", true, data.requestType.soilInfo, { languageId: vietnamId.message, name: "Soil" }));

	let qaCenterId = rootAccount.messageFactory(await rootAccount.sendRequest("addQACenter", true, data.requestType.qaCenterInfo, { languageId: vietnamId.message, name: "qaCenter" }));

	// // Upload test
	// let content = fs.readFileSync('/Users/mac/Downloads/hello_world.pdf');
	// let res = await rootAccount.upload(content);
	// console.log(`uploaded result is ${JSON.stringify(res)}`);

	console.log(`vietnamese has been added at id=${vietnamId.message}`);
	console.log(`english has been added at id=${englishId.message}`);
	console.log(`unit unit1 has been added at id=${unitId.message}`);
	console.log(`new certificate has been added at id=${certificateId.message}`);
	console.log(`new soil has been added at id=${soilId.message}`);
	console.log(`new QACenter has been added at id=${qaCenterId.message}`);

	let unitInfo = await rootAccount.queryData("getUnit", { from: unitId.message, languageId: vietnamId.message });
	console.log(`unitInfo ${JSON.stringify(rootAccount.toObject(data.requestType.unitInfo, unitInfo))}`);

	// init new account
	let owner = new Account(data.config);
	let email = makeEmail(6);
	let password = "123456";

	// register
	let ownerInfo = await owner.register(email, password);
	console.log(`new created account is ${owner.Address} wallet info is ${ JSON.stringify(ownerInfo) }`);

	owner.LanguageID = vietnamId.message;

	let farmInfoData = {
		name: randomText(20),
		address: randomText(20),
		code: randomText(10),
		glnCode: randomText(10),
		area: randomNumbers(1),
		businessLicense: randomText(10),
		email: makeEmail(6),
		phone: randomNumbers(8),
		languageId: vietnamId.message
	};

	// register new farm and add permission
	let result = await owner.registerFarm(farmInfoData);
	if (!result) {
		throw "registerFarmFailed"
	}
	// get farm
	console.log(`new farm has been created at id=${owner.ID}`);
	let farmInfo = await owner.queryData("getFarm", {languageId: owner.LanguageID});
	console.log(`farmInfo ${JSON.stringify(await owner.toObject(data.requestType.farmInfo, farmInfo))}`);

	// Bob register new account
	let bob = new Account(data.config);
	let bobInfo = await bob.register(makeEmail(6), "12345");
	if (bobInfo) {
		console.log(`bob address=${bob.Address} privatekey=${bob.PrivateKey}`);
	} else {
		throw "error occurred while bob register new account";
	}

	// =========== TEST ENABLE / DISABLE PERMISSIONS ======================================================
	// owner gives Bob some permissions.
	await owner.addPermissions({ enabled: ["updateFarm", "getSection"] }, [bob.Address]);

	// get current Bob permission
	await bob.getPermissions(owner.ID);
	console.log(`current permissions of Bob are ${JSON.stringify(bob.Permissions)}`);

	// owner then decides disable updateFarm permission
	// owner gives Bob some permissions.
	await owner.addPermissions({ enabled: ["getSection"], disabled: ["updateFarm"] }, [bob.Address]);

	// get current Bob permission
	await bob.getPermissions(owner.ID);
	console.log(`current permissions of Bob are ${JSON.stringify(bob.Permissions)}`);

	// ==== LIST ALL USERS WHICH BELONG TO CURRENT FARM =====================
	console.log(`get all users that belong to farmId=${owner.ID}`);
	let users = await owner.queryData("getUsersByFarm");
	console.log(`${JSON.stringify(users)}`);

	// === LIST ALL USERS THAT HAVE PERMISSION WITH METHOD getSection
	console.log(`get all users that belong to farmId=${owner.ID} and has permission with method=getSection`);
	let usersByPermission = await owner.queryData("getUsersByPermission", {customs: [{
			"@type": "proto.StringMessage",
			"message": "getSection"
		}]});
	console.log(`${JSON.stringify(usersByPermission)}`);

	return { vietnamese: vietnamId.message, email: email, password: password, unitId: unitId.message, certificateId: certificateId.message, soilId: soilId.message, qaCenterId: qaCenterId.message };
};

const getPesticides = async (result) => {
	// let account = new Account(data.config);
	// await account.login("macnguyenkhoi4gmail.com", "Alo123ma!!");
	let rootAccount = new Account({ address: RootAddress, privateKey: RootPrivateKey, ...data.config });
	let check = await rootAccount.queryData("getPesticideProviders", { fromField: "PesticideId", from: 0, languageId: result.vietnamese, order: { field: "CreatedAt", type: 1 }, customs: [{
			"@type": "proto.Int32Message",
			"message": 1,
		}] });
	console.log(check)
};

const main = async () => {

	let result = await setupMetadata();
	console.log(result);
	// result = { vietnamese: 1, email: "macnguyenkhoi@gmail.com", password: "Alo123ma!!", unitId: result.unitId };
	// login.
	// note that farm must update its name at least 1 language in order to login.
	// let account = new Account(data.config);
	// await account.login(result.email, result.password);
	// console.log(`address=${account.Address}  privateKey=${account.PrivateKey}}`);
	// account.LanguageID = result.vietnamese;
	//
	//
	//
	// // get list of working farms
	// let farms = await account.getWorkingFarms();
	// console.log(`list working farm is ${JSON.stringify(farms)}`);
	// account.ID = farms.data[0].id;
	//
	// // get permissions from first farm
	// await account.getPermissions(farms.data[0].id);
	// console.log(`permissions of address ${account.Address} ID=${account.ID} is ${JSON.stringify(account.Permissions)}`);
	//
	// let rootAccount = new Account({ address: RootAddress, privateKey: RootPrivateKey, ...data.config });
	//
	// let check1 = await rootAccount.sendRequest("addGradedFinishedProductFarm",true, "proto.GradedFinishedProductInfo", {
	// 	gradedId: 1,
	// 	finishedProductFarmId: 1,
	// 	unitId: 1,
	// 	id: 0
	// }
	// );
	//
	// // let check1 = await rootAccount.queryData("getGradedFinishedProductFarms", {
	// // 	fromField: "Id",
	// // 	from: 0,
	// // 	languageId: 1,
	// // 	order: { field: "CreatedAt", type: 1 },
	// // 	customs: [{
	// // 		// "@type": "proto.Int32Message",
	// // 		// "message": 1,
	// // 	}]
	// // 	}
	// // );
	//
  // console.log(check1);
	// // // add section
	// // result = await doSection(account, result);
	// //
	// // // add soilTestResult
	// // result = await doAddSoilTestResult(account, result);
	// //
	// // // Add new farm certificate
	// // await doFarmCertificate(account, result);

};

const doSection = async (account, result) => {
	// add section
	// loop 100 times and add section with random generated fields.
	console.log("sending addSection requests");
	let sectionIds = [];
	for (let i = 0; i < 20; i++) {
		let sectionInfo = {
			name: randomText(6),
			sectionCode: randomText(3),
			glnCode: randomText(10),
			location: randomText(10),
			unitId: result.unitId,
			area: Math.floor(Math.random() * 10),
			manageBy: account.Address,
			phone: randomNumbers(9),
			email: makeEmail(6),
			languageId: result.vietnamese,
			farmId: account.ID
		};
		let sectionId = await account.messageFactory(await account.sendRequest("addSection", true, data.requestType.sectionInfo, sectionInfo));
		sectionIds.push(sectionId.message);
	}
	console.log(JSON.stringify(sectionIds));

	// paging section with limit=10 per page
	let i = 0;
	let counter = 1;
	let from = sectionIds[0];
	while (i < 20) {
		console.log(`page ${counter}`);
		let sectionList = await account.queryData("getSections", { fromField: "Id", from: from, limit: 10, languageId: result.vietnamese, order: { field: "CreatedAt", type: 1 } }); //ASC
		for (let j = 0; j < sectionList.data.length; j++) {
			console.log(account.toObject(data.requestType.sectionInfo, sectionList.data[j]))
		}
		if (sectionList.data.length === 0) {
			break;
		}
		from = sectionList.data[sectionList.data.length - 1].id + 1;
		i += 10;
		counter++;
		console.log("\n\n\n");
	}

	// add arableland
	let arableLandId = await account.messageFactory(await account.sendRequest("addArableLand", true, data.requestType.arableLand, {
		farmId: account.ID,
		sectionId: sectionIds[0],
		area: randomNumbers(2)
	}));
	return {...result, arableLandId: arableLandId.message}
};

const doFarmCertificate = async (account, result) => {
	// Add new farm certificate
	console.log("sending addFarmCertificates requests");
	let farmCertificates = [];
	for (let i = 0; i < 20; i++) {
		let farmCertificateInfo = {
			code: randomText(6),
			certificateId: result.certificateId,
			issuedDate: randomDateStart(),
			expiredDate: randomDateEnd(),
			farmId: account.ID,
			attach: "amz.com/" + randomText(3) + ".pdf"
		};
		console.log(`start adding new farmCertificate - data = ${JSON.stringify(farmCertificateInfo)}`);
		let farmCertificateId = await account.messageFactory(await account.sendRequest("addFarmCertificate", true, data.requestType.farmCertificateInfo, farmCertificateInfo));
		farmCertificates.push(farmCertificateId.message);
	}
	console.log(`created farmCerticates ID = ${JSON.stringify(farmCertificates)}`);

	// paging section with limit=10 per page
	let i = 0;
	let counter = 1;
	let from = farmCertificates[0];
	while (i < 20) {
		console.log(`page ${counter}`);
		let farmCertificatesList = await account.queryData("getFarmCertificates", { fromField: "Id", from: from, limit: 10, languageId: result.vietnamese, order: { field: "CreatedAt", type: 1 } }); //ASC
		for (let j = 0; j < farmCertificatesList.data.length; j++) {
			console.log(JSON.stringify(farmCertificatesList.data[j]))
		}
		if (farmCertificatesList.data.length === 0) {
			break;
		}
		from = farmCertificatesList.data[farmCertificatesList.data.length - 1].id + 1;
		i += 10;
		counter++;
		console.log("\n\n\n");
	}
};

const doAddSoilTestResult = async (account, result) => {
	// request to add arable land
	let soilTestId = await account.messageFactory(await account.sendRequest("addSoilTestResult", true, "proto.SoilTestResultInfo", {
		code : "126",
		soilId : result.soilId,
		soilName : 'soil',
		arableLandId : result.arableLandId,
		sampleDate : { seconds: randomDateStart() },
		sampleBy : 'QA',
		qaCenterId : result.qaCenterId,
		qaCenterName : 'qaCenter',
		issuedAt : { seconds: randomDateStart() },
		languageId : result.languageId,
		farmId: result.farmId
	}));
	console.log(`soilTestResult Id is ${soilTestId.message}`);
	return {...result, soilTestResultId: soilTestId.message};
};

const initAccount= async () => {
	let rootAccount = new Account({ address: RootAddress, privateKey: RootPrivateKey, ...data.config });
	let BytesMessage = rootAccount.Proto.lookupType("proto.BytesMessage");
	let User = rootAccount.Proto.lookupType("proto.User");
	let users = [
		User.fromObject({
			email: "macnguyenkhoi4@gmail.com",
			address: "0x6a3b3F9b2140D2C1B3e01aBcFC542a9ceFe4935b",
			cipher: "aes-128-ctr",
			kdf: "scrypt",
			cipherText: "8b4f988da747de585b0812e3d49f60b92b9a9cdbf498242d4c6817e770742e95",
			iv: "6b81ab9a79145fc5972df0ad25ca491c",
			salt: "00461561e5f3b5673f3a63a80f8517446e3c9ca2ddc0ed0836bb789c079f627b",
			mac: "1d78567399200daffcc3444dec4e934258981367015f530041905cf82f52dd2b",
			timestamp: -2020592146,
			version: 0
		}),
		User.fromObject({
			email: "phuc.nguyen@lina.network",
			address: "0x57A2Cf7530362785a693B60C32cB2019aD779C70",
			cipher: "aes-128-ctr",
			kdf: "scrypt",
			cipherText: "06ffaa721262cfc8d78e8b9c912770e5061684775f9f49c9663e752e2aed4432",
			iv: "5fdaf0b5bb92ecd751952c3776663796",
			salt: "c12b61b93711e6c604f17c4b65da0d6a2af147aecbf14534429d958c2378d7be",
			mac: "0bf6f3316ab1ea1bfd2038f8600cedd79d27a88c371103115b9c6aedc65f64a5",
			timestamp: -2022180234,
			version: 0
		})
	];
	for (let i=0; i < users.length; i++) {
		let input = bytesmessage.fromObject({ message: encodeMesage(User, users[i]) });
		await rootAccount.sendRequest("signup", false, "proto.BytesMessage", bytesmessage.toObject(input))
	}
};

const encodeMesage = (object, message) => {
	return Array.from(object.encode(message).finish());
};

main().then(function () {
	console.log("done");
}).catch(function (err) {
	console.error(err);
});

// let now = new Date();
// let account = new Account();
// let ts = account.Proto.lookupType("google.protobuf.Timestamp");
// console.log(ts.fromObject({
// 	seconds: now.getTime()/1000,
// 	nano: now.getMilliseconds()
// }));
