const Account = require("./lib/account");
const fs = require('fs');

const data = require("./data.json");
const RootPrivateKey = "0x8843ebcb1021b00ae9a644db6617f9c6d870e5fd53624cefe374c1d2d710fd06";
const RootAddress = "0xc1fe56E3F58D3244F606306611a5d10c8333f1f6";

const methods = {
    sc_farm: "register",
    sc_language: "addLanguage",
    sc_seedling: "addSeedling",
    sc_purpose: "addPurpose",
    sc_method_pesticide: "addMethodPesticide",
    sc_care_activities: "addCareActivity",
    sc_weather: "addWeather",
    sc_water: "addWater",
    sc_fertilizer: "addFertilizer",
    sc_finished_product: "addFinishedProduct",
    sc_disease: "addDisease",
    sc_certificate: "addCertificate",
    sc_container_tool: "addContainerTool",
    sc_qa_center: "addQACenter",
    sc_unit: "addUnit",
    sc_graded_product: "addGradedProduct",
    sc_soil: "addSoil",
    sc_provider: "addProvider",
    sc_method_water: "addMethodWater",
    sc_method_waste_treatment: "addMethodWasteTreatment",
    sc_method_fertilizer: "addMethodFertilizer",
    sc_method_care: "addMethodCare",
    sc_category_fertilizer: "addCategoryFertilizer",
    sc_category_pesticide: "addCategoryPesticide",
    sc_storehouse: "addStoreHouse",
    sc_pesticide: "addPesticide",
    sc_origin_seedling: "addOriginSeedling",
    sc_fertilizer_import: "addFertilizerProviderImport",
    sc_pesticide_import: "addPesticideProviderImport",
    sc_permissions: "addPermissions",
    sc_water_test_result: "addWaterTestResult",
    sc_lot_pesticide: "addLotPesticide",
    sc_fertilizer_provider: "addFertilizerProvider",
    sc_farm_certificate: "addFarmCertificate",
    sc_archives: "addArchive",
    sc_graded_finished_product: "addGradedFinishedProductFarm",
    sc_seedlings_provider: "addSeedlingProvider",
    sc_seedling_provider_import: "addSeedlingProviderImport",
    sc_lot_fertilizer: "addLotFertilizer",
    sc_arable_land: "addArableLand",
    sc_finished_product_farm: "addFinishedProductFarm",
    sc_finished_product_test_result: "addFinishedProductTestResult",
    sc_soil_test_result: "addSoilTestResult",
    sc_season: "addSeason",
    sc_lot_seedling: "addLotSeedling",
    sc_section: "addSection",
    sc_pesticide_provider: "addPesticideProvider",
    sc_seedling_cultivation: "addSeedlingCultivation",
    sc_fertilizer_cultivation: "addFertilizerCultivation",
    sc_pesticide_cultivation: "addPesticideCultivation",
    sc_cultivation_test: "addCultivationTest",
    sc_cultivation_test_result: "addCultivationTestResult",
    sc_cultivation_treatment: "addCultivationTreatment",
    sc_water_cultivation: "addWaterCultivation",
    sc_care_history: "addCareHistory",
    sc_care_history_waste_treatment: "addCareHistoryWasteTreatment"
};

const protos = {
    sc_language: "proto.Language",
    sc_seedling: "proto.SeedlingInfo",
    sc_purpose: "proto.PurposeInfo",
    sc_method_pesticide: "proto.MethodPesticideInfo",
    sc_care_activities: "proto.CareActivityInfo",
    sc_weather: "proto.WeatherInfo",
    sc_water: "proto.WaterInfo",
    sc_fertilizer: "proto.FertilizerInfo",
    sc_finished_product: "proto.FinishedProductInfo",
    sc_disease: "proto.DiseaseInfo",
    sc_certificate: "proto.CertificateInfo",
    sc_container_tool: "proto.ContainerToolInfo",
    sc_qa_center: "proto.QACenterInfo",
    sc_unit: "proto.UnitInfo",
    sc_graded_product: "proto.GradedProduct",
    sc_soil: "proto.SoilInfo",
    sc_provider: "proto.ProviderInfo",
    sc_method_water: "proto.MethodWaterInfo",
    sc_method_waste_treatment: "proto.MethodWasteTreatmentInfo",
    sc_method_fertilizer: "proto.MethodFertilizerInfo",
    sc_method_care: "proto.MethodCareInfo",
    sc_category_fertilizer: "proto.CategoryFertilizerInfo",
    sc_category_pesticide: "proto.CategoryPesticideInfo",
    sc_storehouse: "proto.StoreHouseInfo",
    sc_pesticide: "proto.PesticideInfo",
    sc_origin_seedling: "proto.OriginSeedlingInfo",
    sc_farm: "proto.FarmInfo",
    sc_fertilizer_import: "proto.FertilizerProviderImportInfo",
    sc_pesticide_import: "proto.PesticideProviderImportInfo",
    sc_permissions: "proto.PermissionsInfo",
    sc_water_test_result: "proto.WaterTestResultInfo",
    sc_lot_pesticide: "proto.LotPesticideInfo",
    sc_fertilizer_provider: "proto.FertilizerProvider",
    sc_farm_certificate: "proto.FarmCertificateInfo",
    sc_archives: "proto.ArchiveInfo",
    sc_graded_finished_product: "proto.GradedFinishedProductFarmInfo",
    sc_seedlings_provider: "proto.SeedlingProvider",
    sc_seedling_import: "proto.SeedlingProviderImportInfo",
    sc_lot_fertilizer: "proto.LotFertilizerInfo",
    sc_arable_land: "proto.ArableLand",
    sc_finished_product_farm: "proto.FinishedProductFarm",
    sc_finished_product_test_result: "proto.FinishedProductTestResultInfo",
    sc_soil_test_result: "proto.SoilTestResultInfo",
    sc_season: "proto.SeasonInfo",
    sc_lot_seedling: "proto.LotSeedlingInfo",
    sc_section: "proto.SectionInfo",
    sc_pesticide_provider: "proto.PesticideProvider",
    sc_seedling_cultivation: "proto.SeedlingCultivation",
    sc_fertilizer_cultivation: "proto.FertilizerCultivation",
    sc_pesticide_cultivation: "proto.PesticideCultivation",
    sc_cultivation_test: "proto.CultivationTest",
    sc_cultivation_test_result: "proto.CultivationTestResult",
    sc_cultivation_treatment: "proto.CultivationTreatment",
    sc_water_cultivation: "proto.WaterCultivation",
    sc_care_history: "proto.CareHistory",
    sc_care_history_waste_treatment: "proto.CareHistoryWasteTreatment"
};

const [
    LANGUAGE,
    SEEDLING,
    PURPOSE,
    METHOD_PESTICIDE,
    CARE_ACTIVITIES,
    WEATHER,
    WATER,
    FERTILIZER,
    FINISHED_PRODUCT,
    DISEASE,
    CERTIFICATE,
    CONTAINER_TOOL,
    QA_CENTER,
    UNIT,
    GRADE_PRODUCT,
    SOIL,
    PROVIDER,
    METHOD_WATER,
    METHOD_WASTE_TREATMENT,
    METHOD_FERTILIZER,
    METHOD_CARE,
    CATEGORY_FERTILIZER,
    CATEGORY_PESTICIDE,
    STORE_HOUSE,
    PESTICIDE,
    ORIGIN_SEEDLING,
    SC_FARM,
    FERTILIZER_PROVIDER_IMPORT,
    PESTICIDE_PROVIDER_IMPORT,
    PERMISSIONS,
    WATER_TEST_RESULT,
    LOT_PESTICIDE,
    FERTILIZER_PROVIDER,
    FARM_CERTIFICATE,
    ARCHIVE,
    GRADED_FINISHED_PRODUCT_FARM,
    SEEDLING_PROVIDER,
    SEEDLING_PROVIDER_IMPORT,
    LOT_FERTILIZER,
    ARABLE_LAND,
    FINISHED_PRODUCT_FARM,
    FINISHED_PRODUCT_TEST_RESULT,
    SOIL_TEST_RESULT,
    SEASON,
    LOT_SEEDLING,
    SECTION,
    PESTICIDE_PROVIDER,
    SEEDLING_CULTIVATION,
    FERTILIZER_CULTIVATION,
    PESTICIDE_CULTIVATION,
    CULTIVATION_TEST,
    CULTIVATION_TEST_RESULT,
    CULTIVATION_TREATMENT,
    WATER_CULTIVATION,
    CARE_HISTORY,
    CARE_HISTORY_WASTE_TREATMENT
] = Object.keys(protos);

const rootAccount = new Account({ address: RootAddress, privateKey: RootPrivateKey, ...data.config });
let farmOwner = new Account(data.config);

const proto = rootAccount.Proto;
const User = proto.lookupType("proto.User");
const BytesMessage = proto.lookupType("proto.BytesMessage");
const Language = proto.lookupType("proto.Language");

let addedFarm = {};
let addedFinishedProduct = {};
let addedFinishedProductFarm = {};
let addedSection = {};
let addedArchives = {};
let addedFertilizers = {};
let addedPesticides = {};
let addedProviders = {};
let addedSeedlings = {};
let addedArableLands = {};
let addedSeasons = {};
let addedLanguage = {};

const encodeMesage = (object, message) => {
    return Array.from(object.encode(message).finish());
};

const loadFile = (path, name) => {
    let rawdata = fs.readFileSync(path + "/" + name + ".json");
    return JSON.parse(rawdata)["RECORDS"]
};

const importUsers = async (path) => {
    // read the path and get sc_user.json file content
    let users = loadFile(path, "sc_users");
    let now = new Date();

    for (let i=0; i < users.length; i++) {
        let user = User.fromObject({
            email: users[i].email,
            address: users[i].address,
            cipher: users[i].cipher,
            kdf: users[i].kdf,
            cipherText: users[i].cipher_text,
            iv: users[i].iv,
            salt: users[i].salt,
            mac: users[i].mac,
            timestamp: now.getSeconds(),
            version: 0
        });
        let input = BytesMessage.fromObject({ message: encodeMesage(User, user) });
        let result = await rootAccount.sendRequest("createWallet", false, "proto.BytesMessage", BytesMessage.toObject(input));
        if (!result) throw "import user failed"
    }
};

const lookup = (t, obj) => {
    return proto.lookupType(protos[t]).fromObject(obj);
};

const addRequest = async (acc, t, obj, waitForId=false) => {
    return await acc.sendRequest(methods[t], waitForId, protos[t], obj);
};

const updateRequest = async (acc, t, obj) => {
    return await acc.sendRequest(methods[t].replace("add", "update"), false, protos[t], obj);
};

const importLanguage = async (path) => {
    let languages = loadFile(path, LANGUAGE);
    for (let i=0; i < languages.length; i++) {
        let language = Language.fromObject({
            language: languages[i].language
        });
        let result = await rootAccount.sendRequest(methods[LANGUAGE], false, protos[LANGUAGE], language);
        if (!result) throw "import language failed";
    }
};

const importOriginSeedling = async (path) => {
    // load trans table
    let originSeedlingTranses = loadFile(path, ORIGIN_SEEDLING + "_trans");
    for (let i=0; i < originSeedlingTranses.length; i++) {
        let originSeedlingInfo = lookup(ORIGIN_SEEDLING, {
            name: originSeedlingTranses[i].name,
            languageId: originSeedlingTranses[i].language_id
        });
        let result = await addRequest(rootAccount, ORIGIN_SEEDLING, originSeedlingInfo);
        if (!result) throw "import originSeedling failed";
    }
};

const importFinishedProduct = async (path) => {
    let finishedProductsTrans = loadFile(path, FINISHED_PRODUCT + "_trans");
    let finishedProducts = loadFile(path, FINISHED_PRODUCT);
    if (finishedProductsTrans.length < finishedProducts.length) throw "finishedProductTrans < finishedProducts";

    for (let i=0; i < finishedProducts.length; i++) {
        for (let j=0; j < finishedProductsTrans.length; j++) {
            if (finishedProductsTrans[j].finished_product_id === finishedProducts[i].id) {
                let finishedProductInfo = lookup(FINISHED_PRODUCT, {
                    languageId: finishedProductsTrans[j].language_id,
                    name: finishedProductsTrans[j].name,
                    code: finishedProducts[i].code
                });
                if (addedFinishedProduct[finishedProducts[i].id] && addedFinishedProduct[finishedProducts[i].id] > 0) {
                    // do update instead of insert
                    finishedProductInfo.id = addedFinishedProduct[finishedProducts[i].id];
                    finishedProductInfo.status = 1;
                    let result = await updateRequest(rootAccount, FINISHED_PRODUCT, finishedProductInfo);
                    if (!result) throw "update finishedFroduct failed"
                } else {
                    let result = rootAccount.messageFactory(await addRequest(rootAccount, FINISHED_PRODUCT, finishedProductInfo, true));
                    addedFinishedProduct[finishedProducts[i].id] = result.message;
                }
            }
        }
    }
};

const importCommonMaster = async (path, key, transKey, account=undefined) => {

    if (account === undefined) account = rootAccount;

    let trans = loadFile(path, key + "_trans");
    let data = loadFile(path, key);
    if (trans.length < data.length) throw `import ${key}: trans < data`;
    let added = {};
    for (let i=0; i < data.length; i++) {
        for (let j=0; j < trans.length; j++) {
            if (trans[j][transKey] === data[i].id) {
                let info = lookup(key, {
                    languageId: trans[j].language_id,
                    name: trans[j].name,
                });
                if (added[data[i].id] && added[data[i].id] > 0) {
                    // do update instead of insert
                    info.id = added[data[i].id];
                    info.status = 1;
                    let result = await updateRequest(account, key, info);
                    if (!result) throw `update ${key} failed`
                } else {
                    let result = account.messageFactory(await addRequest(account, key, info, true));
                    added[data[i].id] = result.message;
                }
            }
        }
    }
};

const importSeedling = async (path) => {
    let seedlings = loadFile(path, SEEDLING);
    let seedlingsTrans = loadFile(path, SEEDLING + "_trans");

    if (seedlingsTrans.length < seedlings.length) throw "length of seedlingTrans is smaller than seedlings";
    for (let i=0; i<seedlings.length;i++) {
        for (let j=0; j<seedlingsTrans.length; j++) {
            if (seedlingsTrans[j].seedling_id === seedlings[i].id) {
                let seedlingInfo = lookup(SEEDLING, {
                    code: seedlings[i].code,
                    unitId: seedlings[i].unit_id,
                    originSeedlingId: seedlings[i].origin_seedling_id,
                    finishedProductId: seedlings[i].finished_product_id,
                    gtin: seedlings[i].gtin,
                    name: seedlingsTrans[j].name,
                    languageId: seedlingsTrans[j].language_id
                });
                if (addedSeedlings[seedlings[i].id] && addedSeedlings[seedlings[i].id] > 0) {
                    console.log(`do update ${JSON.stringify(seedlingInfo)}`);
                    // do update instead of insert
                    seedlingInfo.id = addedSeedlings[seedlings[i].id];
                    seedlingInfo.status = 1;
                    let result = await updateRequest(rootAccount, SEEDLING, seedlingInfo);
                    if (!result) throw "update seedling failed"
                } else {
                    console.log(`do insert ${JSON.stringify(seedlingInfo)}`);
                    let result = rootAccount.messageFactory(await addRequest(rootAccount, SEEDLING, seedlingInfo, true));
                    addedSeedlings[seedlings[i].id] = result.message;
                }
            }
        }
    }
};

const importGradedProduct = async (path) => {
    let products = loadFile(path, GRADE_PRODUCT);
    for (let i=0; i < products.length; i++) {
        let productInfo = lookup(GRADE_PRODUCT, {
            code: products[i].code,
            name: products[i].name
        });
        let result = await addRequest(rootAccount, GRADE_PRODUCT, productInfo);
        if (!result) throw "import gradedProduct failed";
    }
};

const importCertificate = async (path) => {
    let certs = loadFile(path, CERTIFICATE);
    for (let i=0; i < certs.length; i++) {
        let info = lookup(CERTIFICATE, {
            name: certs[i].name
        });
        let result = await addRequest(rootAccount, CERTIFICATE, info);
        if (!result) throw "import certificate failed";
    }
};

const importPesticide = async (path) => {
    let pesticides = loadFile(path, PESTICIDE);
    let pesticidesTrans = loadFile(path, PESTICIDE + "_trans");

    if (pesticidesTrans.length < pesticides.length) throw "length of pesticidesTrans is smaller than pesticides";
    for (let i=0; i<pesticides.length;i++) {
        for (let j=0; j<pesticidesTrans.length; j++) {
            if (pesticidesTrans[j].pesticide_id === pesticides[i].id) {
                let pesticideInfo = lookup(PESTICIDE, {
                    code: pesticides[i].code,
                    categoryId: pesticides[i].category_id,
                    typeId: pesticides[i].type_id,
                    gtin: pesticides[i].gtin,
                    unitId: pesticides[i].unit_id,
                    unitDisplayId: pesticides[i].unit_display_id,
                    unitWeightId: pesticides[i].unit_weight_id,
                    grossWeight: pesticides[i].gross_weight,
                    netWeight: pesticides[i].net_weight,
                    unitVolumeId: pesticides[i].unit_volume_id,
                    volume: pesticides[i].volume,
                    unitDimensionId: pesticides[i].unit_dimension_id,
                    dimension: pesticides[i].dimension,
                    languageId: pesticidesTrans[j].language_id,
                    name: pesticidesTrans[j].name
                });
                if (addedPesticides[pesticides[i].id] && addedPesticides[pesticides[i].id] > 0) {
                    // do update instead of insert
                    pesticideInfo.id = addedPesticides[pesticides[i].id];
                    pesticideInfo.status = 1;
                    let result = await updateRequest(rootAccount, PESTICIDE, pesticideInfo);
                    if (!result) throw "update pesticide failed"
                } else {
                    let result = rootAccount.messageFactory(await addRequest(rootAccount, PESTICIDE, pesticideInfo, true));
                    addedPesticides[pesticides[i].id] = result.message;
                }
            }
        }
    }
};

const importFertilizer = async (path) => {
    let fertilizers = loadFile(path, FERTILIZER);
    let fertilizersTrans = loadFile(path, FERTILIZER + "_trans");

    if (fertilizersTrans.length < fertilizers.length) throw "length of fertilizersTrans is smaller than fertilizers";
    for (let i=0; i<fertilizers.length;i++) {
        for (let j=0; j<fertilizersTrans.length; j++) {
            if (fertilizersTrans[j].fertilizer_id === fertilizers[i].id) {
                let fertilizerInfo = lookup(FERTILIZER, {
                    code: fertilizers[i].code,
                    categoryId: fertilizers[i].category_id,
                    gtin: fertilizers[i].gtin,
                    unitId: fertilizers[i].unit_id,
                    unitDisplayId: fertilizers[i].unit_display_id,
                    unitWeightId: fertilizers[i].unit_weight_id,
                    grossWeight: fertilizers[i].gross_weight,
                    netWeight: fertilizers[i].net_weight,
                    unitVolumeId: fertilizers[i].unit_volume_id,
                    volume: fertilizers[i].volume,
                    unitDimensionId: fertilizers[i].unit_dimension_id,
                    dimension: fertilizers[i].dimension,
                    languageId: fertilizersTrans[j].language_id,
                    name: fertilizersTrans[j].name
                });
                if (addedFertilizers[fertilizers[i].id] && addedFertilizers[fertilizers[i].id] > 0) {
                    // do update instead of insert
                    fertilizerInfo.id = addedFertilizers[fertilizers[i].id];
                    fertilizerInfo.status = 1;
                    let result = await updateRequest(rootAccount, FERTILIZER, fertilizerInfo);
                    if (!result) throw "update fertilizer failed"
                } else {
                    let result = rootAccount.messageFactory(await addRequest(rootAccount, FERTILIZER, fertilizerInfo, true));
                    addedFertilizers[fertilizers[i].id] = result.message;
                }
            }
        }
    }
};

const importProvider = async (path) => {
    let providers = loadFile(path, PROVIDER);
    let providersTrans = loadFile(path, PROVIDER + "_trans");

    if (providersTrans.length < providers.length) throw "length of providersTrans is smaller than providers";
    for (let i=0; i<providers.length;i++) {
        for (let j=0; j<providersTrans.length; j++) {
            if (providersTrans[j].provider_id === providers[i].id) {
                let providerInfo = lookup(PROVIDER, {
                    businessLicense: providers[i].business_license,
                    gln: providers[i].gln,
                    legal: providers[i].legal,
                    languageId: providersTrans[j].language_id,
                    name: providersTrans[j].name
                });
                if (addedProviders[providers[i].id] && addedProviders[providers[i].id] > 0) {
                    // do update instead of insert
                    providerInfo.id = addedProviders[providers[i].id];
                    providerInfo.status = 1;
                    let result = await updateRequest(rootAccount, PROVIDER, providerInfo);
                    if (!result) throw "update provider failed"
                } else {
                    let result = rootAccount.messageFactory(await addRequest(rootAccount, PROVIDER, providerInfo, true));
                    addedProviders[providers[i].id] = result.message;
                }
            }
        }
    }
};

const login = async (email, password) => {
    await farmOwner.login(email, password);
};

const registerFarm = async (path) => {
    let farms = loadFile(path, SC_FARM);
    let farmsTrans = loadFile(path, SC_FARM + "_trans");

    for (let i=0; i < farms.length; i++) {
        if (farms[i].owner_address !== farmOwner.Address) continue;
        for (let j=0; j < farmsTrans.length; j++) {
            // create farm
            let farmInfo = lookup(SC_FARM, {
                code: farms[i].code,
                glnCode: farms[i].gln_code,
                area: farms[i].area,
                businessLicense: farms[i].business_license,
                email: farms[i].email,
                phone: farms[i].phone,
                owner: farmsTrans[j].owner,
                name: farmsTrans[j].name,
                languageId: farmsTrans[j].language_id
            });
            if (farmOwner.ID > 0) {
                // do update
                let result = await farmOwner.sendRequest("updateFarm", false, protos[SC_FARM], farmInfo);
                if (!result) throw "update farm failed";
            } else {
                let result = farmOwner.messageFactory(await addRequest(farmOwner, SC_FARM, farmInfo, true));
                console.log(`farmID=${result.message}`);
                addedFarm[farms[i].id] = result.message;
                farmOwner.ID = result.message;
            }
        }
    }
};

const importSection = async (path) => {
    console.log(`start import section`);
    let sections = loadFile(path, SECTION);
    let sectionsTrans = loadFile(path, SECTION + "_trans");
    if (sectionsTrans.length < sections.length) throw "length of sectionsTrans is smaller than sections";
    for (let i=0; i<sections.length;i++) {
        for (let j=0; j<sectionsTrans.length; j++) {
            if (sectionsTrans[j].section_id === sections[i].id && addedFarm[sections[i].farm_id] === farmOwner.ID) {
                let sectionInfo = lookup(SECTION, {
                    sectionCode: sections[i].section_code,
                    glnCode: sections[i].gln_code,
                    unitId: sections[i].unit_id,
                    area: sections[i].area,
                    phone: sections[i].phone,
                    email: sections[i].email,
                    manageBy: sections[i].manage_by,
                    location: sectionsTrans[j].location,
                    name: sectionsTrans[j].name,
                    languageId: sectionsTrans[j].language_id
                });
                if (addedSection[sections[i].id] && addedSection[sections[i].id] > 0) {
                    // do update instead of insert
                    sectionInfo.id = addedSection[sections[i].id];
                    sectionInfo.status = 1;
                    let result = await updateRequest(farmOwner, SECTION, sectionInfo);
                    if (!result) throw "update section failed"
                } else {
                    let result = farmOwner.messageFactory(await addRequest(farmOwner, SECTION, sectionInfo, true));
                    addedSection[sections[i].id] = result.message;
                }
            }
        }
    }
};

const importArableLand = async (path) => {
    console.log(`start import arableLand`);
    let arableLands = loadFile(path, ARABLE_LAND);
    for (let i=0; i < arableLands.length; i++) {
        if (addedFarm[arableLands[i].farm_id] === farmOwner.ID && addedSection[arableLands[i].section_id] && addedSection[arableLands[i].section_id] > 0) {
            let arableLand = lookup(ARABLE_LAND, {
                sectionId: addedSection[arableLands[i].section_id],
                sectionLand: arableLands[i].section_land,
                area: arableLands[i].area,
            });
            let result = farmOwner.messageFactory(await addRequest(farmOwner, ARABLE_LAND, arableLand, true));
            if (result.error) throw "add arableLand failed";
            addedArableLands[arableLands[i].id] = result.message;
        }
    }
};

const importArchives = async (path) => {
    console.log(`start import archives`);
    let archives = loadFile(path, ARCHIVE);
    let archivesTrans = loadFile(path, ARCHIVE + "_trans");

    if (archivesTrans.length < archives.length) throw "length of archivesTrans is smaller than archives";
    for (let i=0; i<archives.length;i++) {
        for (let j=0; j<archivesTrans.length; j++) {
            if (archivesTrans[j].section_id === archives[i].id && addedFarm[archives[i].farm_id] === farmOwner.ID) {
                let info = lookup(ARCHIVE, {
                    archivesCode: archives[i].archives_code,
                    purposeId: archives[i].purpose_id,
                    unitId: archives[i].unit_id,
                    area: archives[i].area,
                    location: archivesTrans[j].location,
                    name: archivesTrans[j].name,
                    languageId: archivesTrans[j].language_id
                });
                if (addedArchives[archives[i].id] && addedArchives[archives[i].id] > 0) {
                    // do update instead of insert
                    info.id = addedArchives[archives[i].id];
                    info.status = 1;
                    let result = await updateRequest(farmOwner, ARCHIVE, info);
                    if (!result) throw "update section failed"
                } else {
                    let result = farmOwner.messageFactory(await addRequest(farmOwner, ARCHIVE, info, true));
                    addedArchives[archives[i].id] = result.message;
                }
            }
        }
    }
};

const importFarmCertificate = async (path) => {
    console.log(`start import farm certificate`);
    let farmCertificates = loadFile(path, FARM_CERTIFICATE);
    for (let i=0; i < farmCertificates.length; i++) {
        console.log(`farm_certificate's farm_id=${farmCertificates[i].farm_id} farm'owner id=${farmOwner.ID}`);
        if (addedFarm[farmCertificates[i].farm_id] === farmOwner.ID) {
            let cert = lookup(FARM_CERTIFICATE, {
                certificateId: farmCertificates[i].certificate_id,
                code: farmCertificates[i].code,
                issuedDate: {
                    seconds: Math.floor(Date.parse(farmCertificates[i].issued_date)/1000)
                },
                expiredDate: {
                    seconds: Math.floor(Date.parse(farmCertificates[i].expired_date)/1000)
                },
                issuedBy: farmCertificates[i].issued_by,
                attach: farmCertificates[i].attach
            });
            let result = await addRequest(farmOwner, FARM_CERTIFICATE, cert);
            if (!result) throw "add farm certificate failed"
        }
    }
};

const importWaterTestResult = async (path) => {
    console.log(`start import waterTestResult`);
    let waterTestResults = loadFile(path, WATER_TEST_RESULT);
    for (let i=0; i < waterTestResults.length; i++) {
        if (addedFarm[waterTestResults[i].farm_id] === farmOwner.ID) {
            let info = lookup(WATER_TEST_RESULT, {
                code: waterTestResults[i].code,
                waterId: waterTestResults[i].water_id,
                purposeId: waterTestResults[i].purpose_id,
                sampleDate: {
                    seconds: Math.floor(Date.parse(waterTestResults[i].sample_date)/1000)
                },
                sampleBy: waterTestResults[i].sample_by,
                qaCenterId: waterTestResults[i].qa_center_id,
                issuedAt: {
                    seconds: Math.floor(Date.parse(waterTestResults[i].issuedAt)/1000)
                },
                attach: waterTestResults[i].attach
            });
            let result = await addRequest(farmOwner, WATER_TEST_RESULT, info);
            if (!result) throw "add waterTestResult failed"
        }
    }
};

const importSoilTestResult = async (path) => {
    console.log(`start import soilTestResult`);
    let soilTestResults = loadFile(path, SOIL_TEST_RESULT);
    for (let i=0; i < soilTestResults.length; i++) {
        if (addedFarm[soilTestResults[i].farm_id] === farmOwner.ID && addedArableLands[soilTestResults[i].arable_land_id] !== undefined) {
            let info = lookup(SOIL_TEST_RESULT, {
                code: soilTestResults[i].code,
                soilId: soilTestResults[i].soil_id,
                arableLandId: addedArableLands[soilTestResults[i].arable_land_id],
                sampleDate: {
                    seconds: Math.floor(Date.parse(soilTestResults[i].sample_date)/1000)
                },
                sampleBy: soilTestResults[i].sample_by,
                qaCenterId: soilTestResults[i].qa_center_id,
                issuedAt: {
                    seconds: Math.floor(Date.parse(soilTestResults[i].issuedAt)/1000)
                },
                attach: soilTestResults[i].attach
            });
            let result = await addRequest(farmOwner, SOIL_TEST_RESULT, info);
            if (!result) throw "add soilTestResult failed"
        }
    }
};

const importFinishedProductFarm = async (path) => {
    console.log(`start import finishedProductFarm`);
    let finishedProductFarms = loadFile(path, FINISHED_PRODUCT_FARM);
    for (let i=0; i < finishedProductFarms.length; i++) {
        if (addedFarm[finishedProductFarms[i].farm_id] === farmOwner.ID && addedFinishedProduct[finishedProductFarms[i].finished_product_id] && addedFinishedProduct[finishedProductFarms[i].finished_product_id] > 0) {
            let info = lookup(FINISHED_PRODUCT_FARM, {
                finishedProductId: addedFinishedProduct[finishedProductFarms[i].finished_product_id],
                gtin: finishedProductFarms[i].gtin
            });
            let result = farmOwner.messageFactory(await addRequest(farmOwner, FINISHED_PRODUCT_FARM, info, true));
            addedFinishedProductFarm[finishedProductFarms[i].id] = result.message;
        }
    }
};

const importFinishedProductTestResult = async (path) => {
    console.log(`start import FinishedProductTestResult`);
    let finishedProductTestResults = loadFile(path, FINISHED_PRODUCT_TEST_RESULT);
    for (let i=0; i < finishedProductTestResults.length; i++) {
        if (addedFarm[finishedProductTestResults[i].farm_id] === farmOwner.ID && addedFinishedProduct[finishedProductTestResults[i].finished_product_id] && addedFinishedProduct[finishedProductTestResults[i].finished_product_id] > 0) {
            let info = lookup(FINISHED_PRODUCT_TEST_RESULT, {
                code: finishedProductTestResults[i].code,
                finishedProductId: addedFinishedProduct[finishedProductTestResults[i].finished_product_id],
                sampleDate: {
                    seconds: Math.floor(Date.parse(finishedProductTestResults[i].sample_date)/1000)
                },
                sampleBy: finishedProductTestResults[i].sample_by,
                qaCenterId: finishedProductTestResults[i].qa_center_id,
                issuedAt: {
                    seconds: Math.floor(Date.parse(finishedProductTestResults[i].issuedAt)/1000)
                },
                attach: finishedProductTestResults[i].attach
            });
            let result = await addRequest(farmOwner, FINISHED_PRODUCT_TEST_RESULT, info);
            if (!result) throw "add finishedProductTestResult failed"
        }
    }
};

const importGradedFinishedProduct = async (path) => {
    console.log(`start import gradedFinishedProduct`);
    let gradedFinishedProductFarms = loadFile(path, GRADED_FINISHED_PRODUCT_FARM);
    for (let i=0; i < gradedFinishedProductFarms.length; i++) {
        if (addedFinishedProductFarm[gradedFinishedProductFarms[i].finished_product_farm_id] > 0 && addedFarm[gradedFinishedProductFarms[i].farm_id] === farmOwner.ID) {
            let info = lookup(GRADED_FINISHED_PRODUCT_FARM, {
                finishedProductId: addedFinishedProductFarm[gradedFinishedProductFarms[i].finished_product_farm_id],
                farmId: addedFarm[i].farm_id,
                gtin: gradedFinishedProductFarms[i].gtin
            });
            let result = await addRequest(farmOwner, GRADED_FINISHED_PRODUCT_FARM, info);
            if (!result) throw "add finishedProductTestResult failed"
        }
    }
};

const importSeedlingProviderImport = async (path) => {
    console.log(`start import seedlingProviderImport`);
    let seedlingProviderImports = loadFile(path, SEEDLING_PROVIDER_IMPORT);
    let lotSeedlings = loadFile(path, LOT_SEEDLING);

    for (let i=0; i < seedlingProviderImports.length; i++) {
        for (let j=0; j < lotSeedlings.length; j++) {
            if (addedFarm[seedlingProviderImports[i].farm_id] === farmOwner.ID &&  seedlingProviderImports[i].lot_seedling_id === lotSeedlings[j].id
            && addedArchives[seedlingProviderImports[i].archives_id] && addedArchives[seedlingProviderImports[i].archives_id] > 0) {
                let info = lookup(SEEDLING_PROVIDER_IMPORT, {
                    seedlingProviderId: seedlingProviderImports[i].seedling_provider_id,
                    archivesId: addedArchives[seedlingProviderImports[i].archives_id],
                    importDate: {
                        seconds: Math.floor(Date.parse(seedlingProviderImports[i].import_date)/1000)
                    },
                    quantity: seedlingProviderImports[i].quantity,
                    lotNumber: seedlingProviderImports[i].lot_number,
                    age: seedlingProviderImports[i].age,
                    lotSeedlingName: lotSeedlings[j].name,
                    expirationDate: {
                        seconds: Math.floor(Date.parse(seedlingProviderImports[i].expiration_date)/1000)
                    },
                    lotSeedlingId: lotSeedlings[j].id
                });
                let result = await addRequest(farmOwner, SEEDLING_PROVIDER_IMPORT, info);
                if (!result) throw "add finishedProductTestResult failed"
            }
        }

    }
};

const importPesticideProviderImport = async (path) => {
    console.log(`start import pesticideProviderImport`);
    let pesticideProviderImports = loadFile(path, PESTICIDE_PROVIDER_IMPORT);
    let lotPesticides = loadFile(path, LOT_PESTICIDE);

    for (let i=0; i < pesticideProviderImports.length; i++) {
        for (let j=0; j < lotPesticides.length; j++) {
            if (addedFarm[pesticideProviderImports[i].farm_id] === farmOwner.ID &&  pesticideProviderImports[i].lot_pesticide_id === lotPesticides[j].id
                && addedArchives[pesticideProviderImports[i].archives_id] && addedArchives[pesticideProviderImports[i].archives_id] > 0) {
                let info = lookup(PESTICIDE_PROVIDER_IMPORT, {
                    pesticideProviderId: pesticideProviderImports[i].pesticide_provider_id,
                    archivesId: addedArchives[pesticideProviderImports[i].archives_id],
                    importDate: {
                        seconds: Math.floor(Date.parse(pesticideProviderImports[i].import_date)/1000)
                    },
                    quantity: pesticideProviderImports[i].quantity,
                    quantityDisplay: pesticideProviderImports[i].quantity_display,
                    lotNumber: pesticideProviderImports[i].lot_number,
                    age: pesticideProviderImports[i].age,
                    lotPesticideName: lotPesticides[j].name,
                    expirationDate: {
                        seconds: Math.floor(Date.parse(pesticideProviderImports[i].expiration_date)/1000)
                    },
                    productionDate: {
                        seconds: Math.floor(Date.parse(pesticideProviderImports[i].production_date)/1000)
                    },
                    lotPesticideId: lotPesticides[j].id
                });
                let result = await addRequest(farmOwner, PESTICIDE_PROVIDER_IMPORT, info);
                if (!result) throw "add finishedProductTestResult failed"
            }
        }

    }
};

const importFertilizerProviderImport = async (path) => {
    console.log(`start import fertilizerProviderImport`);
    let fertilizerProviderImports = loadFile(path, FERTILIZER_PROVIDER_IMPORT);
    let lotFertilizers = loadFile(path, LOT_FERTILIZER);

    for (let i=0; i < fertilizerProviderImports.length; i++) {
        for (let j=0; j < lotFertilizers.length; j++) {
            if (addedFarm[fertilizerProviderImports[i].farm_id] === farmOwner.ID &&  fertilizerProviderImports[i].lot_fertilizer_id === lotFertilizers[j].id
            && addedArchives[fertilizerProviderImports[i].archives_id] && addedArchives[fertilizerProviderImports[i].archives_id] > 0) {
                let info = lookup(FERTILIZER_PROVIDER_IMPORT, {
                    fertilizerProviderId: fertilizerProviderImports[i].fertilizer_provider_id,
                    archivesId: addedArchives[fertilizerProviderImports[i].archives_id],
                    importDate: {
                        seconds: Math.floor(Date.parse(fertilizerProviderImports[i].import_date)/1000)
                    },
                    quantity: fertilizerProviderImports[i].quantity,
                    quantityDisplay: fertilizerProviderImports[i].quantity_display,
                    lotNumber: fertilizerProviderImports[i].lot_number,
                    age: fertilizerProviderImports[i].age,
                    lotFertilizerName: lotFertilizers[j].name,
                    expirationDate: {
                        seconds: Math.floor(Date.parse(fertilizerProviderImports[i].expiration_date)/1000)
                    },
                    productionDate: {
                        seconds: Math.floor(Date.parse(fertilizerProviderImports[i].production_date)/1000)
                    },
                    lotFertilizerId: lotFertilizers[j].id
                });
                let result = await addRequest(farmOwner, FERTILIZER_PROVIDER_IMPORT, info);
                if (!result) throw "add fertilizerProviderImports failed"
            }
        }

    }
};

const importPesticideProvider = async (path) => {
    console.log(`start import pesticide provider`);
    let pesticideProviders = loadFile(path, PESTICIDE_PROVIDER);
    for (let i=0; i < pesticideProviders.length; i++) {
        if (addedFarm[pesticideProviders[i].farm_id] === farmOwner.ID
            && addedProviders[pesticideProviders[i].provider_id] !== undefined && addedPesticides[pesticideProviders[i].pesticide_id] !== undefined) {
            let info = lookup(PESTICIDE_PROVIDER, {
                pesticideId: addedPesticides[pesticideProviders[i].pesticide_id],
                providerId: addedProviders[pesticideProviders[i].provider_id]
            });
            let result = await addRequest(farmOwner, PESTICIDE_PROVIDER, info);
            if (!result) throw "add pesticideProviders failed"
        }
    }
};

const importFertilizerProvider = async (path) => {
    console.log(`start import fertilizer provider`);
    let fertilizerProviders = loadFile(path, FERTILIZER_PROVIDER);
    for (let i=0; i < fertilizerProviders.length; i++) {
        if (addedFarm[fertilizerProviders[i].farm_id] === farmOwner.ID
            && addedFertilizers[fertilizerProviders[i].fertilizer_id] !== undefined
            && addedProviders[fertilizerProviders[i].provider_id] !== undefined) {
            let info = lookup(FERTILIZER_PROVIDER, {
                fertilizerId: addedFertilizers[fertilizerProviders[i].fertilizer_id],
                providerId: addedProviders[fertilizerProviders[i].provider_id]
            });
            let result = await addRequest(farmOwner, FERTILIZER_PROVIDER, info);
            if (!result) throw "add pesticideProviders failed"
        }
    }
};

const importSeedlingProvider = async (path) => {
    console.log(`start import seedling provider`);
    let seedlingProviders = loadFile(path, SEEDLING_PROVIDER);
    for (let i=0; i < seedlingProviders.length; i++) {
        if (addedFarm[seedlingProviders[i].farm_id] === farmOwner.ID && addedProviders[seedlingProviders[i].provider_id] !== undefined
            && addedSeedlings[seedlingProviders[i].seedling_id] !== undefined) {
            let info = lookup(SEEDLING_PROVIDER, {
                seedlingId: addedSeedlings[seedlingProviders[i].seedling_id],
                providerId: addedProviders[seedlingProviders[i].provider_id]
            });
            let result = await addRequest(farmOwner, SEEDLING_PROVIDER, info);
            if (!result) throw "add seedlingProviders failed"
        }
    }
};

const importSeason = async (path) => {
    console.log(`start import season`);
    let seasonTrans = loadFile(path, SEASON + "_trans");
    let seasons = loadFile(path, SEASON);
    for (let i=0; i < seasons.length; i++) {
        for (let j=0; j < seasonTrans.length; j++) {
            if (addedFarm[seasons[i].farm_id] === farmOwner.ID && seasonTrans[j].season_id === seasons[i].season_id
                && addedSeedlings[seasons[i].seedling_id] !== undefined && addedArableLands[seasons[i].arable_land_id] !== undefined) {
                let info = lookup(SEASON, {
                    arableLandId: addedArableLands[seasons[i].arable_land_id],
                    seedlingId: addedSeedlings[seasons[i].seedling_id],
                    startDate: {
                        seconds: Math.floor(Date.parse(seasons[i].start_date)/1000)
                    },
                    estimateEndDate: {
                        seconds: Math.floor(Date.parse(seasons[i].estimate_end_date)/1000)
                    },
                    endDate: {
                        seconds: Math.floor(Date.parse(seasons[i].end_date)/1000)
                    },
                    remark: seasons[i].remark,
                    name: seasonTrans[j].name,
                    languageId: seasonTrans[j].language_id
                });

                if (addedSeasons[seasons[i].id] && addedSeasons[seasons[i].id] > 0) {
                    // do update instead of insert
                    info.id = addedSeasons[seasons[i].id];
                    info.status = 1;
                    let result = await updateRequest(farmOwner, SEASON, info);
                    if (!result) throw "update seasons failed"
                } else {
                    let result = farmOwner.messageFactory(await addRequest(farmOwner, SEASON, info, true));
                    addedSeasons[seasons[i].id] = result.message;
                }
            }
        }

    }
};

const importSeedlingCultivation = async (path) => {
    console.log(`start import seedlingCultivation`);
    let seedlingCultivations = loadFile(path, SEEDLING_CULTIVATION);
    for (let i=0; i < seedlingCultivations.length; i++) {
        if (addedSeasons[seedlingCultivations[i].season_id] !== undefined && addedSeedlings[seedlingCultivations[i].seedling_id] !== undefined) {
            let info = lookup(SEEDLING_CULTIVATION, {
                seasonId: addedSeasons[seedlingCultivations[i].season_id],
                seedlingId: addedSeedlings[seedlingCultivations[i].seedling_id],
                lotSeedlingId: seedlingCultivations[i].lot_seedling_id,
                quantity: seedlingCultivations[i].quantity,
                executedDate: {
                    seconds: Math.floor(Date.parse(seedlingCultivations[i].executed_date)/1000)
                },
                lowestProduction: seedlingCultivations[i].lowest_production,
                highestProduction: seedlingCultivations[i].highest_production,
                executedBy: seedlingCultivations[i].executed_by,
                typeId: seedlingCultivations[i].type_id,
                status: seedlingCultivations[i].status,
            });
            console.log(`farmOwnerId=${farmOwner.ID}`);
            let result = await addRequest(farmOwner, SEEDLING_CULTIVATION, info);
            if (!result) throw "add seedlingCultivation failed"
        }
    }
};

const importFertilizerCultivation = async (path) => {
    console.log(`start import fertilizerCultivation`);
    let fertilizerCultivations = loadFile(path, FERTILIZER_CULTIVATION);
    for (let i=0; i < fertilizerCultivations.length; i++) {
        if (addedFertilizers[fertilizerCultivations[i].fertilizer_id] && addedSeasons[fertilizerCultivations[i].season_id]) {
            let info = lookup(FERTILIZER_CULTIVATION, {
                seasonId: addedSeasons[fertilizerCultivations[i].season_id],
                fertilizerId: addedFertilizers[fertilizerCultivations[i].fertilizer_id],
                lotFertilizerId: fertilizerCultivations[i].lot_fertilizer_id,
                methodFertilizerId: fertilizerCultivations[i].method_fertilizer_id,
                quantity: fertilizerCultivations[i].quantity,
                dose: fertilizerCultivations[i].dose,
                executedDate: {
                    seconds: Math.floor(Date.parse(fertilizerCultivations[i].executed_date)/1000)
                },
                executedBy: fertilizerCultivations[i].executed_by,
                status: fertilizerCultivations[i].status,
                farmId: farmOwner.ID
            });
            let result = await addRequest(farmOwner, FERTILIZER_CULTIVATION, info);
            if (!result) throw "add fertilizerCultivation failed"
        }
    }
};

const importPesticideCultivation = async (path) => {
    console.log(`start import pesticideCultivation`);
    let pesticideCultivations = loadFile(path, PESTICIDE_CULTIVATION);
    for (let i=0; i < pesticideCultivations.length; i++) {
        if (addedSeasons[pesticideCultivations[i].season_id] !== undefined && addedPesticides[pesticideCultivations[i].pesticide_id] !== undefined) {
            let info = lookup(PESTICIDE_CULTIVATION, {
                seasonId: pesticideCultivations[i].season_id,
                pesticideId: pesticideCultivations[i].pesticide_id,
                lotPesticideId: pesticideCultivations[i].lot_pesticide_id,
                weatherId: pesticideCultivations[i].weather_id,
                purposeId: pesticideCultivations[i].purpose,
                quantity: pesticideCultivations[i].quantity,
                dose: pesticideCultivations[i].dose,
                executedDate: {
                    seconds: Math.floor(Date.parse(pesticideCultivations[i].executed_date)/1000)
                },
                executedBy: pesticideCultivations[i].executed_by,
                typeId: pesticideCultivations[i].type_id,
                status: pesticideCultivations[i].status,
                farmId: farmOwner.ID
            });
            let result = await addRequest(farmOwner, PESTICIDE_CULTIVATION, info);
            if (!result) throw "add pesticideCultivation failed"
        }
    }
};

const importCultivationTest = async (path) => {
    console.log(`start import cultivationTest`);
    let cultivationTests = loadFile(path, CULTIVATION_TEST);
    for (let i=0; i < cultivationTests.length; i++) {
            let info = lookup(CULTIVATION_TEST, {
                seasonId: cultivationTests[i].season_id,
                diseaseId: cultivationTests[i].disease_id,
                executedDate: {
                    seconds: Math.floor(Date.parse(cultivationTests[i].executed_date)/1000)
                },
                executedBy: cultivationTests[i].executed_by,
                remark: cultivationTests[i].remark,
                status: cultivationTests[i].status,
                farmId: farmOwner.ID
            });
            let result = await addRequest(farmOwner, CULTIVATION_TEST, info);
            if (!result) throw "add cultivationTest failed"
    }
};

const importCultivationTestResult = async (path) => {
    console.log(`start import cultivationTestResult`);
    let cultivationTestResults = loadFile(path, CULTIVATION_TEST_RESULT);
    for (let i=0; i < cultivationTestResults.length; i++) {
            let info = lookup(CULTIVATION_TEST_RESULT, {
                cultivationTestId: cultivationTestResults[i].cultivation_test_id,
                seedlingCultivationId: cultivationTestResults[i].seedling_cultivation_id,
                age: cultivationTestResults[i].age,
                quantity: cultivationTestResults[i].quantity,
                goodQuantity: cultivationTestResults[i].goodQuantity,
                status: cultivationTestResults[i].status
            });
            let result = await addRequest(farmOwner, CULTIVATION_TEST_RESULT, info);
            if (!result) throw "add cultivationTestResult failed"
    }
};

const importCultivationTreatment = async (path) => {
    console.log(`start import cultivationTreatment`);
    let cultivationTreatments = loadFile(path, CULTIVATION_TREATMENT);
    for (let i=0; i < cultivationTreatments.length; i++) {
            let info = lookup(CULTIVATION_TREATMENT, {
                cultivationTestId: cultivationTreatments[i].cultivation_test_id,
                pesticideId: cultivationTreatments[i].pesticide_id,
                dose: cultivationTreatments[i].dose,
                methodPesticideId: cultivationTreatments[i].method_pesticide_id,
                tools: cultivationTreatments[i].tools,
                startDate: {
                    seconds: Math.floor(Date.parse(cultivationTreatments[i].start_date)/1000)
                },
                endDate: {
                    seconds: Math.floor(Date.parse(cultivationTreatments[i].end_date)/1000)
                },
                status: cultivationTreatments[i].status
            });
            let result = await addRequest(farmOwner, CULTIVATION_TREATMENT, info);
            if (!result) throw "add cultivationTreatment failed"
    }
};

const importWaterCultivation = async (path) => {
    console.log(`start import waterCultivation`);
    let waterCultivations = loadFile(path, WATER_CULTIVATION);
    for (let i=0; i < waterCultivations.length; i++) {
            let info = lookup(WATER_CULTIVATION, {
                seasonId: waterCultivations[i].season_id,
                waterId: waterCultivations[i].water_id,
                volume: waterCultivations[i].volume,
                methodWaterId: waterCultivations[i].method_water_id,
                executedBy: waterCultivations[i].executed_by,
                remark: waterCultivations[i].remark,
                executedDate: {
                    seconds: Math.floor(Date.parse(waterCultivations[i].executed_date)/1000)
                },
                status: waterCultivations[i].status,
                farmId: farmOwner.ID
            });
            let result = await addRequest(farmOwner, WATER_CULTIVATION, info);
            if (!result) throw "add waterCultivation failed"
    }
};

const importCareHistory = async (path) => {
    console.log(`start import careHistory`);
    let careHistories = loadFile(path, CARE_HISTORY);
    for (let i=0; i < careHistories.length; i++) {
            let info = lookup(CARE_HISTORY, {
                seasonId: careHistories[i].season_id,
                executedDate: {
                    seconds: Math.floor(Date.parse(careHistories[i].executed_date)/1000)
                },
                executedBy: careHistories[i].executed_by,
                careActivityId: careHistories[i].care_activity_id,
                methodCareId: careHistories[i].method_care_id,
                remark: careHistories[i].remark,
                status: careHistories[i].status,
                farmId: farmOwner.ID
            });
            let result = await addRequest(farmOwner, CARE_HISTORY, info);
            if (!result) throw "add careHistory failed"
    }
};

const importCareHistoryWasteTreatment = async (path) => {
    console.log(`start import careHistoryWasteTreatment`);
    let careHistoryWasteTreatments = loadFile(path, CARE_HISTORY_WASTE_TREATMENT);
    for (let i=0; i < careHistoryWasteTreatments.length; i++) {
            let info = lookup(CARE_HISTORY_WASTE_TREATMENT, {
                careHistoryId: careHistoryWasteTreatments[i].care_history_id,
                methodWasteTreatmentId: careHistoryWasteTreatments[i].method_waste_treatment_id,
                executedDate: {
                    seconds: Math.floor(Date.parse(careHistoryWasteTreatments[i].executed_date)/1000)
                },
                executedBy: careHistoryWasteTreatments[i].executed_by,
                status: careHistoryWasteTreatments[i].status,
            });
            let result = await addRequest(farmOwner, CARE_HISTORY_WASTE_TREATMENT, info);
            if (!result) throw "add careHistoryWasteTreatment failed"
    }
};

const main = async (path, accounts) => {
    await importUsers(path);
    await importLanguage(path);
    await importOriginSeedling(path);
    await importCommonMaster(path, UNIT, "unit_id");
    await importFinishedProduct(path);
    await importSeedling(path);
    await importCommonMaster(path, PURPOSE, "purpose_id");
    await importCommonMaster(path, QA_CENTER, "qa_center_id");
    await importCommonMaster(path, SOIL, "soil_id");
    await importCommonMaster(path, WATER, "water_id");
    await importCommonMaster(path, WEATHER, "weather_id");
    await importCommonMaster(path, METHOD_WATER, "method_water_id");
    await importCommonMaster(path, METHOD_WASTE_TREATMENT, "method_waste_treatment_id");
    await importCommonMaster(path, METHOD_CARE, "method_care_id");
    await importCommonMaster(path, CARE_ACTIVITIES, "care_activity_id");
    await importCommonMaster(path, CONTAINER_TOOL, "container_tool_id");
    await importCommonMaster(path, DISEASE, "disease_id");
    await importCommonMaster(path, METHOD_PESTICIDE, "method_pesticide_id");
    await importCommonMaster(path, METHOD_FERTILIZER, "method_fertilizer_id");
    await importCommonMaster(path, CATEGORY_PESTICIDE, "category_pesticide_id");
    await importCommonMaster(path, CATEGORY_FERTILIZER, "category_fertilizer_id");
    await importGradedProduct(path);
    await importCertificate(path);
    await importPesticide(path);
    await importFertilizer(path);
    await importProvider(path);

    // for (let i=0; i<accounts.length; i++) {
    //     farmOwner.ID = 0;
    //     await login(accounts[i].email, accounts[i].password);
    //     await registerFarm(path);
    //     await importSection(path);
    //     await importArableLand(path);
    //     await importArchives(path);
    //     await importFarmCertificate(path);
    //     await importCommonMaster(path, STORE_HOUSE, "store_house_id", farmOwner);
    //     await importWaterTestResult(path);
    //     await importSoilTestResult(path);
    //     await importFinishedProductFarm(path);
    //     await importFinishedProductTestResult(path);
    //     await importGradedFinishedProduct(path);
    //     await importPesticideProvider(path);
    //     await importFertilizerProvider(path);
    //     await importSeedlingProvider(path);
    //     await importSeedlingProviderImport(path);
    //     await importFertilizerProviderImport(path);
    //     await importPesticideProviderImport(path);
    //     await importSeason(path);
    //     await importSeedlingCultivation(path);
    //     await importFertilizerCultivation(path);
    //     await importPesticideCultivation(path);
    //     await importCultivationTest(path);
    //     await importCultivationTestResult(path);
    //     await importCultivationTreatment(path);
    //     await importWaterCultivation(path);
    //     await importCareHistory(path);
    //     await importCareHistoryWasteTreatment(path);
    // }
};

main("json_data", [
    {
        email: "phuc.nguyen@lina.network",
        password: "123abc**"
    },
    {
        email: "macnguyenkhoi4@gmail.com",
        password: "Alo123ma!!"
    }
]).then(() => {
    console.log('done')
}).catch((err) => {
    console.error(err);
});
